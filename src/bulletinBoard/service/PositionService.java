package bulletinBoard.service;

import static bulletinBoard.utils.CloseableUtil.*;
import static bulletinBoard.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import bulletinBoard.beans.Position;
import bulletinBoard.dao.PositionDao;

public class PositionService {
	public List<Position> getPosition() {
		Connection connection = null;
		try {
			connection = getConnection();
			PositionDao positionDao = new PositionDao();
			List<Position> ret = positionDao.getPosition(connection);
			commit(connection);
			return ret;
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}