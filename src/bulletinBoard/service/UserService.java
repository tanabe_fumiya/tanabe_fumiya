package bulletinBoard.service;

import static bulletinBoard.utils.CloseableUtil.*;
import static bulletinBoard.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import bulletinBoard.beans.User;
import bulletinBoard.dao.EditingDao;
import bulletinBoard.dao.UserDao;
import bulletinBoard.utils.CipherUtil;

public class UserService {
	public void DeletedUpdate(int id, int deleted) {
		Connection connection = null;
		try {
			connection = getConnection();
			UserDao userDao = new UserDao();
			userDao.deletedUpdate(connection, id, deleted);
			commit(connection);
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void update(User editUser, int id) {
		Connection connection = null;
		try {
			connection = getConnection();
			if(editUser.getPassword().isEmpty() == false) {
				String encPassword = CipherUtil.encrypt(editUser.getPassword());
				editUser.setPassword(encPassword);
			}
			EditingDao editingDao = new EditingDao();
			editingDao.update(connection, editUser, id);
			commit(connection);
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		}
	}

	public List<User> getUser() {
		Connection connection = null;
		try {
			connection = getConnection();
			EditingDao editingDao = new EditingDao();
			List<User> ret = editingDao.getUser(connection);
			commit(connection);
			return ret;
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	public User getEditingUser(int id) {
		Connection connection = null;
		try {
			connection = getConnection();
			EditingDao editingDao = new EditingDao();
			User ret = editingDao.getEditingUser(connection, id);
			commit(connection);
			return ret;
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public User getUser(int userId) {

		Connection connection = null;
		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			User user = userDao.getUser(connection, userId);

			commit(connection);

			return user;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	public void register(User user) {
		Connection connection = null;
		try {
			connection = getConnection();
			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);

			UserDao userDao = new UserDao();
			userDao.insert(connection, user);

			commit(connection);
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}