package bulletinBoard.dao;

import static bulletinBoard.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import bulletinBoard.beans.User;
import bulletinBoard.exception.NoRowsUpdatedRuntimeException;
import bulletinBoard.exception.SQLRuntimeException;

public class EditingDao {
	public void update(Connection connection, User editUser, int id) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET ");
			sql.append("login_id =?");
			sql.append(", name =?");
			sql.append(", branch_id =?");
			sql.append(", position_id =?");
			sql.append(", deleted =? ");
			if(editUser.getPassword().isEmpty() == false) {
				sql.append(", password =?");
			}
			sql.append("WHERE id =?");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, editUser.getLoginId());
			ps.setString(2, editUser.getName());
			ps.setInt(3, editUser.getBranchId());
			ps.setInt(4, editUser.getPositionId());
			ps.setInt(5, editUser.getDeleted());
			if(editUser.getPassword().isEmpty() == true) {
				ps.setInt(6, id);
			} else {
				ps.setString(6, editUser.getPassword());
				ps.setInt(7, id);
			}


			int count = ps.executeUpdate();
			if(count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	public User getEditingUser(Connection connection, int id) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT users.id, login_id, password, users.name, branch_id, position_id, deleted, branches.name as branch_name, positions.name as position_name ");
			sql.append("FROM users ");
			sql.append("INNER JOIN branches ");
			sql.append("ON users.branch_id = branches.id ");
			sql.append("INNER JOIN positions ");
			sql.append("ON users.position_id = positions.id ");
			sql.append("WHERE users.id=?");
			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			User ret = toEditingUserList(rs);
			return ret;
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	public List<User> getUser(Connection connection) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT users.id, login_id, password, users.name, branch_id, position_id, deleted, branches.name as branch_name, positions.name as position_name ");
			sql.append("FROM users ");
			sql.append("INNER JOIN branches ");
			sql.append("ON users.branch_id = branches.id ");
			sql.append("INNER JOIN positions ");
			sql.append("ON users.position_id = positions.id ");
			ps = connection.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();
			List<User> ret = toUserList(rs);
			return ret;
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	private User toEditingUserList(ResultSet rs) throws SQLException {
		User user = new User();
		try {
			while(rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String password = rs.getString("password");
				String name = rs.getString("name");
				int branchId = rs.getInt("branch_id");
				int positionId = rs.getInt("position_id");
				int deleted = rs.getInt("deleted");
				String branchName = rs.getString("branch_name");
				String positionName = rs.getString("position_name");

				user.setId(id);
				user.setLoginId(loginId);
				user.setPassword(password);
				user.setName(name);
				user.setBranchId(branchId);
				user.setPositionId(positionId);
				user.setDeleted(deleted);
				user.setBranchName(branchName);
				user.setPositionName(positionName);
			}
			return user;
		} finally {
			close(rs);
		}
	}
	private List<User> toUserList(ResultSet rs) throws SQLException {
		List<User> ret = new ArrayList<User>();
		try {
			while(rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String password = rs.getString("password");
				String name = rs.getString("name");
				int branchId = rs.getInt("branch_id");
				int positionId = rs.getInt("position_id");
				int deleted = rs.getInt("deleted");
				String branchName = rs.getString("branch_name");
				String positionName = rs.getString("position_name");

				User user = new User();
				user.setId(id);
				user.setLoginId(loginId);
				user.setPassword(password);
				user.setName(name);
				user.setBranchId(branchId);
				user.setPositionId(positionId);
				user.setDeleted(deleted);
				user.setBranchName(branchName);
				user.setPositionName(positionName);
				ret.add(user);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
}