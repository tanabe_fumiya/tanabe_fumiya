package bulletinBoard.dao;
import static bulletinBoard.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import bulletinBoard.beans.UserMessage;
import bulletinBoard.exception.SQLRuntimeException;

public class UserMessageDao {
	public  UserMessage getMinInsertDateMessage(Connection connection) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM user_message ");
			sql.append("WHERE insert_date <= all (SELECT insert_date FROM user_message)");
			ps = connection.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();
			List<UserMessage> ret = toUserMessageList(rs);
			return ret.get(0);
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	public UserMessage getMaxInsertDateMessage(Connection connection) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM user_message ");
			sql.append("WHERE insert_date >= all (SELECT insert_date FROM user_message)");
			ps = connection.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();
			List<UserMessage> ret = toUserMessageList(rs);
			return ret.get(0);
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	public List<UserMessage> getUserMessage(Connection connection, String category, String insertDate1, String insertDate2) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM user_message ");
			sql.append("WHERE category=? ");
			sql.append("AND insert_date ");
			sql.append("BETWEEN ? and ? ");
			sql.append("ORDER BY insert_date DESC");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, category);
			ps.setString(2, insertDate1 + " 00:00:00");
			ps.setString(3, insertDate2 + " 23:59:59");

			ResultSet rs = ps.executeQuery();
			List<UserMessage> ret = toUserMessageList(rs);
			return ret;
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	public List<UserMessage> getUserMessage(Connection connection, String insertDate1, String insertDate2) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM user_message ");
			sql.append("WHERE insert_date ");
			sql.append("BETWEEN ? and ? ");
			sql.append("ORDER BY insert_date DESC");
			ps = connection.prepareStatement(sql.toString());
			if(insertDate1.isEmpty() == true) {
				ps.setString(1, "2019-01-01- 00:00:00");
			} else {
				ps.setString(1, insertDate1 + "00:00:00");
			}
			if(insertDate2.isEmpty() == true) {
				Calendar cal = Calendar.getInstance();
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				String strDate = sdf.format(cal.getTime());
				ps.setString(2, strDate);
			} else {
				ps.setString(2, insertDate2 + " 23:59:59");
			}
			ResultSet rs = ps.executeQuery();
			List<UserMessage> ret = toUserMessageList(rs);
			return ret;
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public List<UserMessage> getCategory(Connection connection, int num) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM user_message GROUP BY category ");
			sql.append("ORDER BY insert_date DESC limit " + num);
			ps = connection.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();
			List<UserMessage> ret = toUserMessageList(rs);
			return ret;
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public List<UserMessage> getUserMessage(Connection connection, int num, String category) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM user_message ");
			sql.append("WHERE category=? ");
			sql.append("ORDER BY insert_date DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, category);

			ResultSet rs = ps.executeQuery();
			List<UserMessage> ret = toUserMessageList(rs);
			return ret;
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	public List<UserMessage> getUserMessage(Connection connection, int num) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM user_message ");
			sql.append("ORDER BY insert_date DESC limit " + num);
			ps = connection.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();
			List<UserMessage> ret = toUserMessageList(rs);
			return ret;
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserMessage> toUserMessageList(ResultSet rs) throws SQLException {
		List<UserMessage> ret = new ArrayList<UserMessage>();
		try{
			while(rs.next()) {
				int id = rs.getInt("id");
				String title = rs.getString("title");
				String text = rs.getString("text");
				String category = rs.getString("category");
				Timestamp insertDate = rs.getTimestamp("insert_Date");
				String name = rs.getString("name");

				UserMessage message = new UserMessage();
				message.setId(id);
				message.setTitle(title);
				message.setText(text);
				message.setCategory(category);
				message.setInsertDate(insertDate);
				message.setName(name);

				ret.add(message);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
}