package bulletinBoard.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bulletinBoard.beans.Branch;
import bulletinBoard.beans.Position;
import bulletinBoard.beans.User;
import bulletinBoard.exception.NoRowsUpdatedRuntimeException;
import bulletinBoard.service.BranchService;
import bulletinBoard.service.PositionService;
import bulletinBoard.service.UserService;

@WebServlet(urlPatterns = { "/editing" })
public class EditingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		HttpSession session = request.getSession();
		User loginUser = (User) session.getAttribute("loginUser");
		List<Branch> branches = new BranchService().getBranch(); 
		List<Position> positions = new PositionService().getPosition();
		//インスタンス化
		List<String> messages = new ArrayList<String>();


		//役職番号"3"（人事総務部）のみ編集権限あり
		if (loginUser.getPositionId() == 3) {
			if (request.getParameter("id") != null && request.getParameter("id").matches("^\\d+$")) {
				int id = Integer.parseInt(request.getParameter("id"));
				User editingUser = new UserService().getEditingUser(id);
				//リクエストスコープにインスタンス(第2引数)を保存
				request.setAttribute("loginUser", loginUser);
				request.setAttribute("editingUser", editingUser);
				request.setAttribute("branches", branches);
				request.setAttribute("positions", positions);
				//フォワード先を"/editing.jsp"に指定し、処理を移す
				request.getRequestDispatcher("/editing.jsp").forward(request, response);
				
			} else {
				//役職番号"1","2","4"は編集権限なし
				messages.add("編集権限がありません。");
				session.setAttribute("errorMessages", messages);
				response.sendRedirect("./");
			}

		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		List<String> messages = new ArrayList<String>();
		User editingUser = getEditUser(request);
		List<Branch> branches = new BranchService().getBranch();
		List<Position> positions = new PositionService().getPosition();
		if (isValid(request, messages) == true) {
			try {
				int id = Integer.parseInt(request.getParameter("id"));
				new UserService().update(editingUser, id);
			} catch (NoRowsUpdatedRuntimeException e) {
				request.setAttribute("positions", positions);
				request.setAttribute("branches", branches);
				request.setAttribute("editingUser", editingUser);
				messages.add("第三者によって更新されています。データを確認してください");
				request.getRequestDispatcher("/editing.jsp").forward(request, response);
			}
			response.sendRedirect("userList");
		} else {
			request.setAttribute("positions", positions);
			request.setAttribute("branches", branches);
			request.setAttribute("editingUser", editingUser);
			request.setAttribute("errorMessage", messages);
			request.getRequestDispatcher("/editing.jsp").forward(request, response);
		}
	}

	private User getEditUser(HttpServletRequest request) throws IOException, ServletException {
		//request.getParameter()メソッドでリクエストパラメータの値を取得
		int id = Integer.parseInt(request.getParameter("id"));
		User editingUser = new User();
		editingUser.setId(id);
		editingUser.setLoginId(request.getParameter("login_id"));
		editingUser.setName(request.getParameter("name"));
		editingUser.setPassword(request.getParameter("password"));
		int branchId = Integer.parseInt(request.getParameter("branch_id"));
		editingUser.setBranchId(branchId);
		int positionId = Integer.parseInt(request.getParameter("position_id"));
		editingUser.setPositionId(positionId);
		return editingUser;
	}

	//編集中のバリデーション
	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String loginId = request.getParameter("login_id");
		String name = request.getParameter("name");
		String password = request.getParameter("password");
		String passwordConfirm = request.getParameter("passwordConfirm");

		if (StringUtils.isEmpty(loginId) == true) {
			messages.add("ログインIDを入力してください");
		} else if (!loginId.matches("^\\w{6,20}$")) {
			messages.add("ログインIDは6文字以上20文字以下で入力してください");
		}
		if (StringUtils.isEmpty(name) == true) {
			messages.add("名前を入力してください");
		}
		if (StringUtils.isEmpty(password) == false) {
			if (!password.matches("^[a-zA-Z0-9 -/:-@\\[-\\`\\{-\\~]+$")) {
				messages.add("パスワードは記号を含む半角英数字で入力してください");
			} else if (password.length() >= 21 || password.length() <= 5) {
				messages.add("6文字以上20文字以下で入力してください");
			} else if (!password.equals(passwordConfirm)) {
				messages.add("パスワードと確認用パスワードが一致しません");
			}
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}