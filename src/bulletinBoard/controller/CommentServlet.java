package bulletinBoard.controller;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bulletinBoard.beans.Comment;
import bulletinBoard.beans.User;
import bulletinBoard.service.CommentService;


@WebServlet(urlPatterns = {"/comments"})
public class CommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{

		request.getRequestDispatcher("home.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		//セッションスコープの取得
		HttpSession session = request.getSession();
		List<String> comments = new ArrayList<String>();
		//リクエストパラメータの取得
		int messageId = Integer.parseInt(request.getParameter("message_id"));
		
		if(isValid(request, comments) == true) {
			//セッションスコープからインスタンスの取得
			User user = (User)session.getAttribute("loginUser");
			Comment comment = new Comment();

			comment.setText(request.getParameter("text"));
			comment.setUserId(user.getId());
			comment.setMessageId(messageId);

			new CommentService().register(comment);
			response.sendRedirect("./");
		} else {
			session.setAttribute("errorMessages", comments);
			response.sendRedirect("./");
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> comments) {
		String comment = request.getParameter("text");
		//コメントが空の場合のバリデーション
		if(StringUtils.isEmpty(comment) == true) {
			comments.add("コメントが入力されていません。コメントを入力して下さい。");
		}
		//コメントが500文字を超えた場合のバリデーション
		if(500 < comment.length()) {
			comments.add("文字数の制限を越えました。500文字以下で入力して下さい。");
		}
		if(comments.size() ==0) {
			return true;
		} else {
			return false;
		}
	}

}
