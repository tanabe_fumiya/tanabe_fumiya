package bulletinBoard.controller;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bulletinBoard.beans.Message;
import bulletinBoard.beans.User;
import bulletinBoard.service.MessageService;

@WebServlet(urlPatterns = {"/newMessage"})
public class NewMessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{

		request.getRequestDispatcher("messages.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
		HttpSession session = request.getSession();
		List<String> messages = new ArrayList<String>();
		List<String> titles = new ArrayList<String>();
		List<String> categories = new ArrayList<String>();

		if(isValid(request, messages, titles, categories) == true) {
			User user = (User)session.getAttribute("loginUser");
			Message message = new Message();

			message.setName(user.getName());
			message.setUserId(user.getId());
			message.setText(request.getParameter("message"));
			message.setCategory(request.getParameter("category"));
			message.setTitle(request.getParameter("title"));

			new MessageService().register(message);
			response.sendRedirect("./");
		} else {
			session.setAttribute("errorMessages", messages);
			request.getRequestDispatcher("messages.jsp").forward(request, response);
		}

	}
	private boolean isValid(HttpServletRequest request, List<String> messages, List<String> titles, List<String> categories) {
		String message = request.getParameter("message");
		String title = request.getParameter("title");
		String category = request.getParameter("category");
		//項目欄が空の場合の処理
		if(StringUtils.isEmpty(category) == true) {
			messages.add("カテゴリーを入力してください");
		}
		if(StringUtils.isEmpty(title) == true) {
			messages.add("タイトルを入力してください");
		}
		if(StringUtils.isEmpty(message) == true) {
			messages.add("メッセージを入力してください");
		}
		//本文が1000文字を超えた場合の処理
		if(1000 < message.length()) {
			messages.add("本文は1000文字以下で入力してください");
		}
		if(messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}