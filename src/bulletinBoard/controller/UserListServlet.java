package bulletinBoard.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bulletinBoard.beans.User;
import bulletinBoard.exception.NoRowsUpdatedRuntimeException;
import bulletinBoard.service.UserService;

@WebServlet(urlPatterns = {"/userList"})
public class UserListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();
		User loginUser = (User)session.getAttribute("loginUser");

		//役職番号"3"人事総務部のみ編集権限あり
		if(loginUser.getPositionId() == 3) {
			List<User> userList = new UserService().getUser();
			request.setAttribute("userLists", userList);
			request.setAttribute("loginUser", loginUser);
			request.getRequestDispatcher("userList.jsp").forward(request, response);
		} else {
			//役職番号"1","2","4"は編集権限なし
			List<String> messages = new ArrayList<String>();
			messages.add("権限をもっていないため、ユーザー管理は使用できません。");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("./");
		}
	}
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		List<String> messages = new ArrayList<String>();
		try {
			int id = Integer.parseInt(request.getParameter("id"));
			int deleted = Integer.parseInt(request.getParameter("deleted"));
			new UserService().DeletedUpdate(id, deleted);

		} catch(NoRowsUpdatedRuntimeException e) {
			messages.add("他の人によって更新されています");
			request.setAttribute("errorMessages", messages);
			response.sendRedirect("userList");
		}
		response.sendRedirect("userList");
	}

}