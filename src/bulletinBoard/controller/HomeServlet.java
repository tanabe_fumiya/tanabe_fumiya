package bulletinBoard.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import bulletinBoard.beans.Comment;
import bulletinBoard.beans.UserMessage;
import bulletinBoard.service.CommentService;
import bulletinBoard.service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		List<UserMessage> categorys = new MessageService().getCategory();
		List<Comment> comments = new CommentService().getComment();

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String minInsertDate = request.getParameter("min_insert_date");
		String maxInsertDate = request.getParameter("max_insert_date");
		System.out.println(maxInsertDate);

		if (minInsertDate == null || minInsertDate.isEmpty() == true) {
			UserMessage minInsertDateMessage = new MessageService().getMinInsertDateMessage();
			minInsertDate = sdf.format(minInsertDateMessage.getInsertDate());
		}
		if (maxInsertDate == null || maxInsertDate.isEmpty() == true) {
			UserMessage maxInsertDateMessage = new MessageService().getMaxInsertDateMessage();
			maxInsertDate = sdf.format(maxInsertDateMessage.getInsertDate());
		}

		if (request.getParameter("category") == null || request.getParameter("category").equals("all")) {
			List<UserMessage> messages = new MessageService().getMessage(minInsertDate, maxInsertDate);
			request.setAttribute("messages", messages);
		} else {
			String getCategory = request.getParameter("category");
			List<UserMessage> messages = new MessageService().getMessage(getCategory, minInsertDate, maxInsertDate);
			request.setAttribute("getCategory", getCategory);
			request.setAttribute("messages", messages);
		}

		request.setAttribute("minInsertDate", minInsertDate);
		request.setAttribute("maxInsertDate", maxInsertDate);
		request.setAttribute("categorys", categorys);
		request.setAttribute("comments", comments);
		request.getRequestDispatcher("/home.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		if (StringUtils.isEmpty(request.getParameter("deleteMessage")) == false) {
			int deleteMessageId = Integer.parseInt(request.getParameter("deleteMessage"));
			new MessageService().deleteMessage(deleteMessageId);
		}
		if (StringUtils.isEmpty(request.getParameter("deleteComment")) == false) {
			int deleteCommentId = Integer.parseInt(request.getParameter("deleteComment"));
			new CommentService().deleteComment(deleteCommentId);
		}
		response.sendRedirect("./");
	}

}