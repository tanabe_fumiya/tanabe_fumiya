package bulletinBoard.controller;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bulletinBoard.beans.User;
import bulletinBoard.service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        request.getRequestDispatcher("signup.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        List<String> messages = new ArrayList<String>();

        HttpSession session = request.getSession();
        if (isValid(request, messages) == true) {

            User user = new User();
            user.setLoginId(request.getParameter("loginId"));
    		user.setName(request.getParameter("name"));
    		user.setPassword(request.getParameter("password"));
    		user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
    		user.setPositionId(Integer.parseInt(request.getParameter("positionId")));

            new UserService().register(user);

            response.sendRedirect("./");
        } else {
            session.setAttribute("errorMessages", messages);
            response.sendRedirect("/signup.jsp");
        }
    }


    private boolean isValid(HttpServletRequest request, List<String> messages) {
		String loginId = request.getParameter("loginId");
		String name = request.getParameter("name");
		String password = request.getParameter("password");
		String passwordConfirm = request.getParameter("passwordConfirm");
		if(StringUtils.isEmpty(loginId) == true) {
			messages.add("ログインIDを入力してください");
		} else if(!loginId.matches("^\\w{6,20}$")) {
			messages.add("ログインIDは6文字以上20文字以下で入力してください");
		}

		if(StringUtils.isEmpty(name) == true) {
			messages.add("名前を入力してください");
		}
		if(StringUtils.isEmpty(password) == true) {
			messages.add("パスワードを入力してください");
		} else if(!password.matches("^[a-zA-Z0-9 -/:-@\\[-\\`\\{-\\~]+$")) {
			messages.add("記号を含む半角英数字で入力してください");
		} else if(password.length() >= 256 || password.length() <= 5 ) {
			messages.add("パスワードは6文字以上20文字以下で入力してください");
		} else if(!password.equals(passwordConfirm)) {
			messages.add("パスワードと確認用パスワードが一致しません");
		}
		if(messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}

