package bulletinBoard.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bulletinBoard.beans.User;
import bulletinBoard.service.LoginService;

@WebServlet(urlPatterns = {"/login"})
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	 @Override
	    protected void doGet(HttpServletRequest request,
	            HttpServletResponse response) throws IOException, ServletException {

	        request.getRequestDispatcher("login.jsp").forward(request, response);
	    }
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		List<String> messages = new ArrayList<String>();
		LoginService loginService = new LoginService();
		HttpSession session = request.getSession();
		User user = loginService.login(loginId, password);

		if(user != null) {
			//現在停止中のアカウントでログイン操作した場合の処理（使用中：0　停止中：1）
			if(user.getDeleted() == 1) {
				messages.add("このユーザーは使用停止中です");
				session.setAttribute("errorMessages", messages);
				response.sendRedirect("login");
			} else {
				session.setAttribute("loginUser", user);
				response.sendRedirect("./");
			}
		} else {
			//アカウント登録無の場合、ID及びPW不一致の場合の処理
			messages.add("ログインに失敗しました");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("login");
		}
	}
}