<%@ page isELIgnored = "false" %>
<%@ taglib prefix ="c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<style type="text/css">
	table {
		border-collapse: collapse;
	}
	th{
		border: solid 1px;
		padding: 0.5em;
	}
</style>
<script type="text/javascript">
<!--
function disp(){
	if(window.confirm('このユーザーの使用状況を変更しますか？')){
		document.frm.submit();
	}
	else{
		return false;
	}
}

// -->
</script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー管理</title>
</head>
<body>
<div class="main-contents">
	<h2>ユーザー管理</h2>
<div class="header">
	<a href="signup">ユーザーの新規登録</a>
	<a href="./">戻る</a><br />
</div>
	<c:if test = "${not empty errorMessages}">
	<div class = "errorMessages">
		<ul>
			<c:forEach items = "${errorMessages}" var = "message">
				<li><c:out value = "${message}"/>
			</c:forEach>
		</ul>
	</div>
	<c:remove var = "errorMessages" scope = "session"/>
</c:if>

<br />
	<table class="user_list" border="1">
		<tr>
			<th>名前</th>
			<th>ログインID</th>
			<th>所属</th>
			<th>役職</th>
			<th>アカウント使用状況</th>
			<th>編集</th>
		</tr>
		<c:forEach items="${userLists}" var="user">
			<tr>
				<td class="name"><c:out value="${user.name}" /></td>
				<td class="login_id"><c:out value="${user.loginId}" /></td>
				<td class="branch_name"><c:out value="${user.branchName}" /></td>
				<td class="position_name"><c:out value="${user.positionName}" /></td>
				<td class="deleted">

				<form name="frm" action="userList" method="post" onSubmit="return disp()">
					<input type="hidden" name="id" value="${user.id}">
					<input type="hidden" name="deleted" value="${user.deleted}">
					<c:if test="${user.id != loginUser.id}">
						<c:if test="${user.deleted == 0}">
							使用中 <input type="submit" value="停止する" name="deleted">
						</c:if>
						<c:if test="${user.deleted ==1}">
							停止中 <input type="submit" value="再開する" name="deleted">
						</c:if>
					</c:if>
					<c:if test="${user.id == loginUser.id}">ログイン中</c:if>
				</form>
				</td>
				<td class="editing_id"><a href="editing?id=${user.id}">編集</a></td>
			</tr>
		</c:forEach>
	</table>
</div>
<div class="copyright">Copyright(c)Tanabe Fumiya</div>
</body>
</html>


