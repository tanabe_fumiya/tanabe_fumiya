<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>ユーザー登録</title>
    </head>
    <body>
        <div class="main-contents">
            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>
            <form action="signup" method="post">
                <br /> <label for="loginId">ログイン名</label> <input name="loginId" id="loginId" />（名前はあなたの公開プロフィールに表示されます）<br />

                <label for="name">名</label> <input name="name"
                    id="name" /> <br /> <label for="password">パスワード</label> <input
                    name="password" type="password" id="password" /> <br />

                    <label for="passwordConfirm">確認パスワード</label> <input
                    name="passwordConfirm" type="password" id="passwordConfirm" /> <br />
所属<br>
<select name="branchId">
<option value="1">本社</option>
<option value="2">支店A</option>
<option value="3">支店B</option>
<option value="4">支店C</option>
</select>

役職<br>
<select name="positionId">
<option value="1">支店長</option>
<option value="2">社員</option>
<option value="3">総務人事部</option>
<option value="4">情報管理部</option>
</select>


                	<input type="submit" value="登録" /> <br /> <a href="./">戻る</a>
            </form>
            
        </div>
        <div class="copyright">Copyright(c)Tanabe Fumiya</div>
    </body>
</html>


