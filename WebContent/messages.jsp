<%@ page isELIgnored = "false" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix ="c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>新規投稿画面</title>
</head>
<body>
<div class="main-contents">
<h2>新規投稿画面</h2>
<div class="header">
	<a href = "./">戻る</a>
</div>
<div class="form-area">


<c:if test="${not empty errorMessages}">
	<div class="errorMessages">
		<ul>
			<c:forEach items="${errorMessages}" var="messages">
				<li><c:out value="${messages}"/>
			</c:forEach>
		</ul>
	</div>
	<c:remove var="errorMessages" scope="session"/>
</c:if>
	<form action="newMessage" method="post"><br />
		<label for = "category">カテゴリー</label>
		<input name = "category" size="10" maxlength="10" id ="category"/>(10文字まで)<br />
		<label for = "title">タイトル</label>
		<input name = "title" size="30" maxlength="30" id ="title"/>(30文字まで)<br />
		本文<br />
		<textarea name="message" rows="10" cols="100" class="messageBox"></textarea><br />
		<br />
		<input type="submit" value="投稿">(1000文字まで)
	</form>
	
</div>
</div>
<div class="copyright">Copyright(c)Tanabe Fumiya</div>
</body>
</html>