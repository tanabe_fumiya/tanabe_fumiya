<%@ page isELIgnored = "false" %>
<%@ taglib prefix ="c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix ="fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>社内掲示板</title>
</head>
<body>

<div class="main-contents">
<h2>社内掲示板</h2>
<div class="header">
		<a href="newMessage">新規投稿画面</a>
		<a href="userList">ユーザー管理</a>
		<a href="logout">ログアウト</a>
</div>
<c:if test = "${not empty errorMessages}">
	<div class = "errorMessages">
		<ul>
			<c:forEach items = "${errorMessages}" var = "message">
				<li><c:out value = "${message}"/>
			</c:forEach>
		</ul>
	</div>
	<c:remove var = "errorMessages" scope = "session"/>
</c:if>
<c:if test="${not empty loginUser}">
	<div class="profile">
		<div class="name">
			<h2><c:out value="${loginUser.name}"/></h2></div>
		<div class="loginId">
			@<c:out value="${loginUser.loginId}"/>
		</div>
		<div class="branch">
			<c:out value="${loginUser.branchName}"/>
		</div>
		<div class="position">
			<c:out value="${loginUser.positionName}"/>
		</div><br />
	</div>
</c:if>
<br />
<br />

<div class="messages">
	<form action="./">
		カテゴリー：<select name="category" class="category" size="1">
			<option value="all">すべて</option>
			<c:forEach items="${categorys}" var="category">
				<c:if test="${category.category == getCategory}">
					<option value="${category.category}" selected >${category.category}</option>
				</c:if>
				<c:if test="${category.category != getCategory}">
					<option value="${category.category}">${category.category}</option>
				</c:if>
			</c:forEach>
		</select>
		<label>
			日付：<input name="min_insert_date" type="date" min="2019-04-01" value="${minInsertDate}">
			から<input name="max_insert_date" type="date" max="2999-03-31" value="${maxInsertDate}">
		</label>
		<input type="submit" value="検索">
	</form><br />


	<c:forEach items="${messages}" var="message">
			<div class="message">
				<div class="category">カテゴリー：<c:out value="${message.category}"/></div>
				<div class="name">投稿者：<c:out value="${message.name}"/></div>
				<div class="title">タイトル：<c:out value="${message.title}" /></div>
				<div class="text">本文：

				<c:forEach var="str" items="${fn:split(message.text,'')}">
				<div><c:out value="${str}" /></div>
				</c:forEach>
				</div>
				<div class="date">投稿日：<fmt:formatDate value="${message.insertDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div>

				<br />
				<div class="comment">
				<c:forEach items="${comments}" var="comment">
					<c:if test="${message.id == comment.messageId}">
						<div class="userId">コメント投稿者：<c:out value="${comment.name}"></c:out></div>
						<div class="text">コメント：<c:out value="${comment.text}"></c:out></div>
						<div class="date">投稿日：<fmt:formatDate value="${comment.insertDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
					</c:if>
				</c:forEach>
				</div><br />
				<form action="comments" method="post">
					<textarea name="text" rows="5" cols="50" class="comments"></textarea><br />
					<input type="hidden" name="message_id" value="${message.id}">
					<input type="submit" value="コメントする">(500文字まで)
				</form>
				<br />
			</div><br />
	</c:forEach>

</div>
</div>
<div class="copyright">Copyright(c)Tanabe Fumiya</div>
</body>
</html>