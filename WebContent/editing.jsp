<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${loginUser.name}のユーザー編集</title>
</head>
<body>

	<div class="main-contents">
		<h2>ユーザ編集</h2>
		<div class="header">
			<a href="userList">戻る</a>
		</div>
		<br />
		<c:if test="${not empty errorMessage}">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessage}" var="messages">
						<li><c:out value="${messages}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
			<br />
		</c:if>
		<br />
		<div class="editing">
			<form action="editing" method="post">
				<label for="login_id">ログインID</label> <input name="login_id"
					value="${editingUser.loginId}" id="login_id" />(半角英数文字で6文字以上20文字以下)<br />

				<label for="name">名前</label> <input name="name"
					value="${editingUser.name}" maxlength="10" id="name" />(10文字以下)<br />

				<label for="password">パスワード</label> <input name="password"
					type="password" id="password" />(記号を含む全ての半角文字で6文字以上20文字以下)<br />
				<label for="passwordConfirm">パスワード(確認)</label> <input
					name="passwordConfirm" type="password" id="passwordConfirm" />(パスワードをもう一度入力してください)<br />

				<c:if test="${loginUser.id != editingUser.id}">
					<div class="branch">
						支店 <select name="branch_id" size="1">
							<c:forEach items="${branches}" var="branch">
								<c:if test="${branch.id == editingUser.branchId}">
									<option value="${branch.id}"
										selected="${editingUser.branchName}">${branch.name}</option>
								</c:if>
								<c:if test="${branch.id != editingUser.branchId}">
									<option value="${branch.id}">${branch.name}</option>
								</c:if>
							</c:forEach>
						</select><br />
					</div>
					<div class="position">
						役職 <select name="position_id" size="1">
							<c:forEach items="${positions}" var="position">
								<c:if test="${position.id == editingUser.positionId}">
									<option value="${position.id}"
										selected="${editingUser.positionName}">${position.name}</option>
								</c:if>
								<c:if test="${position.id != editingUser.positionId}">
									<option value="${position.id}">${position.name}</option>
								</c:if>
							</c:forEach>
						</select><br />
					</div>
				</c:if>
				<c:if test="${loginUser.id == editingUser.id}">
					<div class="branch">
						支店：
						<c:out value="${editingUser.branchName}"></c:out>
					</div>
					<div class="position">
						役職：
						<c:out value="${editingUser.positionName}"></c:out>
					</div>
					<input type="hidden" name="branch_id"
						value="${editingUser.branchId}">
					<input type="hidden" name="position_id"
						value="${editingUser.positionId}">
				</c:if>


				<input type="hidden" name="id" value="${editingUser.id}"> <br />
				<input type="submit" value="更新" />
			</form>
		</div>
	</div>
	<div class="copyright">Copyright(c)Tanabe Fumiya</div>
</body>
</html>